package si.janm.qrserver;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import si.janm.qrserver.core.AppSettings;

/**
 * Main QR Server android application class.
 */
public class TheApplication extends Application {

    public static final int DEFAULT_PORT = 3030;
    private static final String PREFS_KEY = "QrServerPrefs";

    private static TheApplication _instance;
    private Context context;
    private SharedPreferences preferences;
    private PackageInfo packageInfo;

    /**
     * Singleton application class.
     */
    public static synchronized TheApplication getInstance(){
        if (_instance == null) _instance = new TheApplication();
        return _instance;
    }
    public void onCreate() {
        super.onCreate();
        TheApplication app = TheApplication.getInstance();

        app.context = getApplicationContext();
        app.preferences = getApplicationContext().getSharedPreferences(PREFS_KEY, 0);

        try {
            app.packageInfo = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Static application context.
     */
    public Context getAppContext() {
        return this.context;
    }

    /**
     * Methods for manipulating shared preferences.
     */
    public SharedPreferences getSharedPreferences(){
        return this.preferences;
    }
    public AppSettings getSettings(){
        AppSettings settings = new AppSettings();
        settings.port = this.getSharedPreferences().getInt("port", DEFAULT_PORT);
        settings.webhookUrl = this.getSharedPreferences().getString("webhookUrl", null);
        settings.webhookToken = this.getSharedPreferences().getString("webhookToken", null);
        return settings;
    }
    public void saveSettings(AppSettings settings){
        SharedPreferences.Editor editor = this.getSharedPreferences().edit();
        editor.putInt("port", settings.port);
        editor.putString("webhookUrl", settings.webhookUrl);
        editor.putString("webhookToken", settings.webhookToken);
        editor.apply();
    }

    /**
     * Get package info object.
     */
    public PackageInfo getPackageInfo(){
        return this.packageInfo;
    }

}
