package si.janm.qrserver.core;

import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import si.janm.qrserver.TheApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Random;

public class Utl {

    private static boolean LOGS_ENABLED = true;
    private static String LOGS_TAG = "mydebug";
    private static String RAND_POOL = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    /**
     * Write message to developer console.
     */
    public static void log(Object msg){
        if (!LOGS_ENABLED) return;
        String s = (msg != null)? msg.toString() : "null";
        Log.d(LOGS_TAG, s);
    }

    /**
     * Show simple toast message with static context.
     */
    public static void toast(String msg){
        Toast.makeText(TheApplication.getInstance().getAppContext(), msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * Try to parse int from string or return a default value.
     */
    public static int tryParseInt(String str, Integer def){
        Integer r = def;
        try {
            r = Integer.parseInt(str);
        }catch (Exception ignored){ }
        return r;
    }

    /**
     * Generate sha256 hash from given string.
     */
    public static String sha256(String str) {
        String hash = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] bytes = digest.digest(str.getBytes(StandardCharsets.UTF_8));
            StringBuilder builder = new StringBuilder(bytes.length*2);
            for (byte b : bytes) builder.append(String.format("%02x", b & 0xFF));
            hash = builder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hash;
    }

    /**
     * Read input stream to string.
     */
    public static String readIs(InputStream is){
        BufferedReader reader = null;
        String content = null;

        try {
            reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line = reader.readLine();
            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = reader.readLine();
            }
            content = sb.toString();
        }catch (Exception e){

        }finally {
            try {
                if (reader != null) reader.close();
            } catch (IOException ignored) { }
        }

        return content;
    }

    /**
     * Generate random string of length n
     */
    public static String randStr(int n) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();

        for (int i = 0; i < n; i++) {
            sb.append(RAND_POOL.charAt(random.nextInt(RAND_POOL.length())));
        }

        return sb.toString();
    }

    /**
     * Convert byte array to url-safe base64 string
     */
    public static String base64Encode(byte[] bytes) {
        return Base64.encodeToString(bytes, Base64.URL_SAFE).replaceAll("\n$", "");
    }

}
