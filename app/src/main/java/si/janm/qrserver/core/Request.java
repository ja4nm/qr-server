package si.janm.qrserver.core;

import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class Request {

    private HttpURLConnection con;

    public void setMethod(String method) throws ProtocolException {
        this.con.setRequestMethod(method);
    }

    public void setUrl(String urlStr) throws IOException {
        URL url = new URL(urlStr);
        this.con = (HttpURLConnection) url.openConnection();
        this.con.setInstanceFollowRedirects(true);
    }

    public void setTimeouts(int connect, int read){
        this.con.setConnectTimeout(connect);
        this.con.setReadTimeout(read);
    }

    public void setJsonBody(Object obj, Gson gson) throws IOException {
        this.con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

        String body = gson.toJson(obj);

        OutputStreamWriter writer = new OutputStreamWriter(this.con.getOutputStream(), StandardCharsets.UTF_8);
        writer.write(body);
        writer.close();
    }

    public void setJsonBody(Object obj) throws IOException {
        this.setJsonBody(obj, new Gson());
    }

    public String send() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(this.con.getInputStream()));
        String line;
        StringBuilder sb = new StringBuilder();

        while((line = br.readLine()) != null){
            sb.append(line);
        }

        br.close();
        return sb.toString();
    }

    public int sendSafe() throws IOException {
        BufferedInputStream reader = new BufferedInputStream(this.con.getInputStream());
        byte[] buff = new byte[1024*10];
        while (reader.read(buff, 0, buff.length) > 0);
        return this.con.getResponseCode();
    }

}
