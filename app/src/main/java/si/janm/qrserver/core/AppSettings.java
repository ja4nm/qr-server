package si.janm.qrserver.core;

public class AppSettings {

    public int port;
    public String webhookUrl;
    public String webhookToken;

}
