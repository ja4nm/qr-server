package si.janm.qrserver.core;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import si.janm.qrserver.TheApplication;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import fi.iki.elonen.NanoHTTPD;

public class WebServer extends NanoHTTPD {

    private String text;
    private String hash;
    private String webhookInfo;
    private String template;

    /**
     * Http server for QR code scanner.
     *
     * @param port: listen on port.
     */
    public WebServer(int port) {
        super(port);

        this.text = "";
        this.hash = "";
        this.template = "";

        // read preview.html template
        try {
            InputStream is = TheApplication.getInstance().getAppContext().getAssets().open("display.html");
            this.template = Utl.readIs(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Start the server on specified port.
     */
    public void serve() throws IOException {
        this.start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
    }

    /**
     * Main server request handler.
     */
    @Override
    public Response serve(IHTTPSession session) {
        String uri = session.getUri();

        switch (uri){
            case "/":
                return this.handleRoot();
            case "/data":
                return this.handleData();
            case "/hash":
                return this.handleHash();
        }

        return newFixedLengthResponse("");
    }

    /**
     * Set content to display.
     */
    public void publish(String text) {
        this.hash = Utl.sha256(text);
        this.text = text;
    }

    /**
     * Set webhook debug info
     */
    public void publishWebhokInfo(String text) {
        this.webhookInfo = text;
    }

    /**
     * Handle endpoint /
     */
    private Response handleRoot(){
        return newFixedLengthResponse(this.template);
    }

    /**
     * Handle endpoint /raw
     */
    private Response handleData(){
        HashMap<String, Object> data = new HashMap<>();
        data.put("text", this.text);
        data.put("hash", this.hash);
        data.put("webhookInfo", this.webhookInfo);

        GsonBuilder builder = new GsonBuilder().serializeNulls();
        Gson gson = builder.create();
        NanoHTTPD.Response res = newFixedLengthResponse(gson.toJson(data));
        res.addHeader("Content-Type", "application/json");
        return res;
    }

    /**
     * Handle endpoint /hash
     */
    private Response handleHash(){
        NanoHTTPD.Response res = newFixedLengthResponse(this.hash);
        res.addHeader("Content-Type", "text/plain");
        return res;
    }

}
