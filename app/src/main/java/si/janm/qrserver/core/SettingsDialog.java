package si.janm.qrserver.core;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import si.janm.qrserver.R;
import si.janm.qrserver.TheApplication;
import si.janm.qrserver.activities.HelpActivity;

public class SettingsDialog extends AlertDialog {

    private View view;

    private EditText inputPort;
    private EditText inputWebhookUrl;
    private EditText inputWebhookToken;
    private View btnGenerateToken;
    private OnSaveListener onSaveListener;
    private Activity activity;

    /**
     * Settings dialog constructor.
     */
    public SettingsDialog(Activity activity) {
        super(activity);
        this.activity = activity;

        this.view = View.inflate(getContext(), R.layout.dialog_settings, null);
        this.inputPort = this.view.findViewById(R.id.inputPort);
        this.inputWebhookUrl = this.view.findViewById(R.id.inputWebhookUrl);
        this.inputWebhookToken = this.view.findViewById(R.id.inputWebhookToken);
        this.btnGenerateToken = this.view.findViewById(R.id.btnGenerateToken);

        this.btnGenerateToken.setOnClickListener(new OnGenerateTokenClick());

        this.setTitle("Settings");
        this.setButton(Dialog.BUTTON_POSITIVE, "Save", new VoidClickListener());
        this.initAbout();
        this.setView(this.view);
    }


    /**
     * Some setters.
     */
    public void setOnSaveListener(OnSaveListener listener){
        this.onSaveListener = listener;
    }


    /**
     * On dialog open.
     */
    @Override
    public void show() {
        // load settings
        AppSettings settings = TheApplication.getInstance().getSettings();
        if (settings.port > 0) this.inputPort.setText(""+settings.port);
        if (settings.webhookUrl != null) this.inputWebhookUrl.setText(settings.webhookUrl);
        if (settings.webhookToken != null) this.inputWebhookToken.setText(settings.webhookToken);

        super.show();
        this.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(new OnSaveClick());
    }

    /**
     * Initialise dialog about section.
     */
    private void initAbout(){
        String appName = getContext().getResources().getText(R.string.app_name).toString();
        String versionName = TheApplication.getInstance().getPackageInfo().versionName;
        String link = "https://janm.si";
        String authorHtml = String.format("by Jan M - <a href=\"%s\">%s</a>", link, link);

        TextView textAppVersion = this.view.findViewById(R.id.appVersion);
        textAppVersion.setText(String.format("%s v%s ", appName, versionName));

        TextView textAppAuthor = this.view.findViewById(R.id.appAuthor);
        textAppAuthor.setText(Html.fromHtml(authorHtml));
        textAppAuthor.setMovementMethod(LinkMovementMethod.getInstance());

        TextView textHelp = this.view.findViewById(R.id.textHelp);
        textHelp.setText(Html.fromHtml("<a href=\"#\">Help page ...</a>"));
        textHelp.setOnClickListener(new OnLearnMoreClick());
    }

    /**
     * On save button click
     */
    private class OnSaveClick implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int port = Utl.tryParseInt(inputPort.getText().toString(), -1);
            String webhookUrl = inputWebhookUrl.getText().toString();
            String webhookToken = inputWebhookToken.getText().toString();

            // validate input
            if (port < 10 || port > 99999){
                Utl.toast("Invalid port number.");
                return;
            }

            // save settings
            AppSettings settings = new AppSettings();
            settings.port = port;
            settings.webhookUrl = webhookUrl;
            settings.webhookToken = webhookToken;
            TheApplication.getInstance().saveSettings(settings);

            // save
            dismiss();
            if (onSaveListener != null) onSaveListener.onSave(settings);
        }
    }

    /**
     * On learn more text click
     */
    private class OnLearnMoreClick implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            getContext().startActivity(new Intent(activity, HelpActivity.class));
        }
    }

    /**
     * On generate token button click
     */
    private class OnGenerateTokenClick implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            String token = Utl.randStr(16);
            inputWebhookToken.setText(token);
        }
    }

    /**
     * Just do nothing
     */
    private static class VoidClickListener implements OnClickListener{
        @Override
        public void onClick(DialogInterface dialog, int which) {}
    }

    /**
     * Helper event listeners.
     */
    public static abstract class OnSaveListener{
        public abstract void onSave(AppSettings settings);
    }

}
