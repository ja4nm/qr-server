package si.janm.qrserver.core;

import android.os.Build;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class Webhook {

    private String url;
    private WebhookBody body;

    public Webhook(){
        this.body = new WebhookBody();
        this.body.deviceManufacturer = Build.MANUFACTURER;
        this.body.deviceModel = Build.MODEL;
    }

    public Webhook(AppSettings appSettings){
        this();
        this.url = appSettings.webhookUrl;
        this.body.token = appSettings.webhookToken;
    }

    public void setResult(Result result){
        this.body.text = result.getText();
        if (result.getRawBytes() != null)
            this.body.rawBase64 = Utl.base64Encode(result.getRawBytes());

        this.body.timestamp = result.getTimestamp();
        this.body.meta = new HashMap<>();
        if (result.getResultMetadata() != null){
            Map<ResultMetadataType, Object> meta = result.getResultMetadata();
            for (Map.Entry<ResultMetadataType, Object> e : meta.entrySet()){
                this.body.meta.put(e.getKey().name(), e.getValue());
            }
        }
    }

    public int trigger() throws IOException {
        if (this.url == null || this.url.length() == 0) return -1;

        GsonBuilder gsonBuilder = new GsonBuilder();
        Type type = new TypeToken<byte[]>() {}.getType();
        gsonBuilder.registerTypeAdapter(type, new Base64Serializer());
        Gson gson = gsonBuilder.create();

        Request req = new Request();
        req.setUrl(this.url);
        req.setTimeouts(3000, 5000);
        req.setMethod("POST");
        req.setJsonBody(this.body, gson);
        return req.sendSafe();
    }

    /**
     * Post request struct.
     */
    public static class WebhookBody {
        public String text;
        public String rawBase64;
        public long timestamp;
        public HashMap<String, Object> meta;

        public String token;
        public String deviceModel;
        public String deviceManufacturer;
    }

    /**
     * Custom byte array serializer (to base 64)
     */
    private static class Base64Serializer implements JsonSerializer<byte[]> {
        @Override
        public JsonElement serialize(byte[] src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(Utl.base64Encode(src));
        }
    }

}
