package si.janm.qrserver.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import si.janm.qrserver.core.Utl;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utl.log("Just a sample test activity.");
    }
}
