package si.janm.qrserver.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.pm.PackageManager;
import android.os.Bundle;
import com.google.zxing.Result;
import si.janm.qrserver.core.AppSettings;
import si.janm.qrserver.core.QrScannerView;
import si.janm.qrserver.R;
import si.janm.qrserver.core.SettingsDialog;
import si.janm.qrserver.TheApplication;
import si.janm.qrserver.core.Utl;
import si.janm.qrserver.core.WebServer;

public class MainActivity extends AppCompatActivity {

    private QrScannerView qrScannerView;
    private WebServer webServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppSettings settings = TheApplication.getInstance().getSettings();
        this.webServer = new WebServer(settings.port);

        this.qrScannerView = findViewById(R.id.qrScannerView);
        this.qrScannerView.setWebServer(this.webServer);
        this.qrScannerView.setOnDecodeListener(new OnDecode());
        this.qrScannerView.setOnSaveListener(new OnSettingsSave());

        boolean ok = this.requestPermissions();
        if (ok) {
            this.qrScannerView.start();
        }
    }

    /**
     * When qr code is scanned.
     */
    private class OnDecode extends QrScannerView.OnDecodeListener{
        @Override
        public void decoded(Result result, int webhookStatus) {
            final String str = result.getText();

            // show toast preview
            final String msg = (str.length() > 100)? str.substring(0, 100)+" ..." : str;
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Utl.toast(msg);
                }
            });

            // copy to clipboard
            MainActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("QrServer", str);
                    clipboard.setPrimaryClip(clip);
                }
            });
        }
    }

    /**
     * After settings are saved.
     */
    private class OnSettingsSave extends SettingsDialog.OnSaveListener {
        @Override
        public void onSave(AppSettings settings) {
            qrScannerView.stop();
            webServer = new WebServer(settings.port);
            qrScannerView.setWebServer(webServer);
            qrScannerView.start();
        }
    }

    /**
     * Request camera permission.
     */
    private boolean requestPermissions(){
        // we already have the permission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        // we do not have the permission yet
        ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, 100);
        this.qrScannerView.setMessage("You must enable camera permission, to use this app.");
        return false;
    }

    /**
     * When permission is granted, start QR server.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults[0] == 0){
            this.qrScannerView.start();
        }
    }

    /**
     * When activity closes, stop the server.
     */
    @Override
    public void finish() {
        super.finish();
        this.webServer.stop();
    }

}
