package si.janm.qrserver.activities;

import android.os.Bundle;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;

import si.janm.qrserver.R;

public class HelpActivity extends AppCompatActivity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        this.setTitle("Help");

        this.webView = findViewById(R.id.webView);
        this.webView.loadUrl("file:///android_asset/help.html");
    }
}
