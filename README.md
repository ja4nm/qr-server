# QR Server android app

By Jan M (http://janm.si), Nov 12 2020

QR Server is a simple android app which scans a QR code, starts a minimal HTTP web server and displays
a content of the QR code on a local website. As long as your PC is connected to the same Wi-Fi network,
you can then access that website from your PC and copy the content. The HTTP server has 3 endpoints:

* http://yourLocalIp:3030/ - User-friendly page with QR code content, which is updated in real time.
* http://yourLocalIp:3030/data - Data about the last scanned QR code in JSON format.
* http://yourLocalIp:3030/hash - Sha256 hash of last scanned QR code.

## Release notes

Note: folder `./builds` should contain debug apk for each version.


**Version:** v1.1 **Date:** Feb 19, 2021 **File:** `qr-server-v1.1-release.apk`

- Added a webhook functionality. The ability to send an HTTP POST request when a QR code is scanned.
- The request will contain the content and other data about the QR code, which has been scanned. Optionally, you can set a token value to identify the client, from which a QR code has been scanned.
- Added a help page.


**Version:** v1.0 **Date:** Nov 12, 2020 **File:** `qr-server-v1.0-debug.apk`

- First release version.
- Send a QR code content from your phone to your PC in real time.
- Open a link in a QR code on your PC.
- Make your SEPA payments more convenient and less prone to errors. You can scan a QR code on a SEPA payment form and copy the data into your online banking app.


**Version:** v1.0 **Date:** Nov 12, 2020 **File:** `qr-server-v1.0-debug.apk`

- First debug version.


## Screenshots

<img src="./othr/readme/scan_sepa.jpg" alt="Scan screen" width="250" />

<img src="./othr/readme/settings.jpg" alt="Settings screen" width="250" />

<img src="./othr/readme/client_sepa.png" alt="Web client" width="250" />

## Legal documents

**License**

QR Server app is a free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3.0 of the
License, or (at your option) any later version.

QR Server app is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with software. If not, see <http://www.gnu.org/licenses/>.

**Other**

https://janm.si/other/qr-server/legal.php
